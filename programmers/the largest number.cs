using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Solution {
    public string solution(int[] numbers) {
        int size = numbers.Length;
        List<string> ordered = new List<string>(size);
        int zeroCount = 0;
        for (int i = 0; i < size; ++i)
        {
            if (numbers[i] == 0)
            {
                ++zeroCount;
            }
            ordered.Add(numbers[i].ToString());
        }
        
        if (zeroCount == size)
        {
            return "0";
        }
        
        StringBuilder answer = new StringBuilder(1024);
        ordered.Sort((x, y) => (y + x).CompareTo(x + y));
        
        for (int i = 0; i < size; ++i)
        {
            answer.Append(ordered[i]);
        }
        
        return answer.ToString();
    }
}

/*
[프로그래머스]
#코딩테스트 > 레벨2 > 가장 큰 수

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

size_t s_size = 0;

void my_strcpy(char* arr, const int* n1, const int* n2)
{
    char temp[8] = "";
    sprintf(temp, "%d", *n1);
    strcat(arr, temp);
    
    sprintf(temp, "%d", *n2);
    strcat(arr, temp);
}

int compare(const int* a, const int* b)
{
    char ab[9] = "";//한 원소 최댓값이 1000으로 4글자+4글자+null문자로 9
    char ba[9] = "";
    char* ap = ab;
	char* bp = ba;
    
    my_strcpy(ab, a, b);//여기서 두 원소 순서를 바꿔 문자열로 만들고
    my_strcpy(ba, b, a);//아래 while 문에서 strcmp처럼 아스키코드 순서를 알아냄
    s_size += strlen(ab);
    
    while (*ap == *bp && *ab != 0) {
        if (*ap != *bp) {
            return *ap > *bp ? -1 : 1;//내림차순이 맞으니 -1
        }
        ap++;
        bp++;
    }
    
    if (*ap == *bp) {
        return 0;
    }
    return *ap > *bp ? -1 : 1;
}

char* solution(int numbers[], size_t numbers_len) {
    char* answer;
    size_t i = 0;
    char temp[8] = "";//배열 한 원소의 최대 자릿수는 4자리지만 2 제곱수 맞출 겸 크기 8로 지정
    qsort(numbers, numbers_len, sizeof(int), compare);
    
    if (*numbers == 0) {//정렬 후 맨 앞의 원소 값이 0이면? 원소 전부 다 0이니 0반환
        answer = (char*)malloc(sizeof(char) * 2);
        answer[0] = '0';
        answer[1] = '\0';
        return answer;
    }
    answer = (char*)malloc(sizeof(char) * (s_size + 1));
    *answer = 0;//프로그래머스에서는 이렇게 초기화 안 해 주면 쓰레기 값 때문에 오류남
    
    for (i = 0; i < numbers_len; ++i) {
        sprintf(temp, "%d", numbers[i]);
        strcat(answer, temp);
    }
    
    return answer;
}
*/
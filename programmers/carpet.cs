using System;

public class Solution {
    public void dfs(int height, int brown, int yellow, int[] arr)
    {//height가 노란색 격자 양옆 감싸는 갈색 격자 수. 초기화 값은 2, 재귀할 때마다 2씩 추가
        int inner = ((brown - height) / 2 - 2) * (height / 2);
        if (inner == yellow)
        {
            arr[0] = (brown - height) / 2;
            arr[1] = height / 2 + 2;
        }
        else
        {
            dfs(height + 2, brown, yellow, arr);
        }//이렇게 2씩 추가
    }
    
    public int[] solution(int brown, int yellow) {
        int[] answer = new int[2];
        
        dfs(2, brown, yellow, answer);
        return answer;
    }
}
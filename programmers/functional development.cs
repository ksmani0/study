using System;
using System.Diagnostics;
using System.Collections.Generic;

public class Solution {
    public int bucket = 0;
    public int startIndex = 0;
    public int backIndex = 0;
    public int queueSize = 0;//여기까지 4개 요소가 큐에 기본적으로 따라옴
    int[] dayQueue;//이게 가능함!

    public int dequeue()
    {
        Debug.Assert(queueSize != 0);//C의 assert()와 헷갈리지 말자

        int result = dayQueue[startIndex];
        --queueSize;

        startIndex = (startIndex + 1) % bucket;
        return result;
    }

    public int[] solution(int[] progresses, int[] speeds) {
        int size = speeds.Length;
        bucket = size;
        queueSize = size;
        dayQueue = new int[size];

        for (int i = 0; i < size; ++i)
        {
            int dayCount = (100 - progresses[i]) / speeds[i];
            dayCount = (100 - progresses[i]) % speeds[i] == 0 ? dayCount : dayCount + 1;
            dayQueue[i] = dayCount;
        }

        List<int> answer = new List<int>(size);
        int max = dequeue();
        int count = 0;
        for (int i = 1; i < size; ++i)
        {
            int value = dequeue();
            if (max < value)
            {
                answer.Add(count + 1);
                count = 0;
                max = value;
                continue;
            }
            ++count;
        }
        answer.Add(count + 1);

        return answer.ToArray();
    }
}

/*
[프로그래머스]
#코딩테스트 > 레벨2 > 기능 개발
*/
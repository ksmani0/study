using System;

public class Solution {
    public int solution(string skill, string[] skill_trees) {
        int answer = 0;

        for (int i = 0; i < skill_trees.Length; ++i)
        {
            int matchIndex = 0;
            bool match = true;

            for (int j = 0; j < skill_trees[i].Length; ++j)
            {
                for (int k = 0; k < skill.Length; ++k)
                {
                    if (skill_trees[i][j] == skill[k] && k == matchIndex)
                    {
                        ++matchIndex;
                    }
                    else if (skill_trees[i][j] == skill[k] && k != matchIndex)
                    {
                        match = false;
                        goto next_i;
                    }
                }
            }

            next_i:
            if (match)
            {
                ++answer;
            }
        }

        return answer;
    }
}

/*
[프로그래머스]
#코딩테스트 > 레벨2 > 스킬트리
*/
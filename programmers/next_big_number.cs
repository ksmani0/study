using System;

class Solution 
{
    public bool isTwoSquare(int num)
    {//2의 제곱수인지 판단
        if (num == 0)
        {
            return false;
        }
        while (num % 2 == 0)
        {
            num /= 2;
        }        
        return num == 1 ? true : false;
    }
    
    public int countOne(int n)
    {
        int count = 0;
        while (n != 0)
        {//get reversed binary
            count = n % 2 != 0 ? count + 1 : count;
            n /= 2;
        }
        return count;
    }
    
    public int solution(int n) {
        if (isTwoSquare(n) == true) {
            return n * 2;//2의 제곱수면 2 곱한 다음 수 반환하면 됨
        }
        else if (isTwoSquare(n + 1) == true)
        {//모든 비트가 1일 땐 다다음 2 제곱수 수한 후 이전 2 제곱수 빼면 됨
            int nextSqure = n + 1;
            return (nextSqure * 2 - 1) - nextSqure / 2;
        }
        else if (n == 0)
        {//0은 1이 하나도 없으니 그대로 0 반환
            return 0;
        }
        
        int oneCount = countOne(n);
        
        int nextNum = n;
        while (true)
        {
            nextNum++;//1씩 추가하며 n과 1 수 같은 것 찾기
            if (countOne(nextNum) == oneCount)
            {
                break;
            }
        }
        
        return nextNum;
    }
}
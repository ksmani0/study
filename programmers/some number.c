[프로그래머스]
#코딩테스트 > 연습문제 > K번째 수

using System;

public class Solution {
    public int[] solution(int[] array, int[,] commands)
    {
        int rowLength = commands.GetLength(0);
        int[] answer = new int[rowLength];
        
        for (int i = 0; i < rowLength; ++i)
        {
            int[] orderArray = new int[commands[i,1] - commands[i,0] + 1];
            int k = 0;
            for (int j = commands[i,0] - 1; j < commands[i,1]; ++j)
            {
                orderArray[k++] = array[j];
            }
            
            for (int j = 0; j < orderArray.Length - 1; ++j)
            {
                if (orderArray[j] > orderArray[j + 1])
                {
                    int tmp = orderArray[j];
                    orderArray[j] = orderArray[j + 1];
                    orderArray[j + 1] = tmp;
                    j = -1;
                }
            }
            
            answer[i] = orderArray[commands[i,2] - 1];
        }//array[i][j] 이런 형태는 배열의 배열로 2차원 배열이 아님!
        
        return answer;
    }
}

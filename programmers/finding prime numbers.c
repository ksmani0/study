public class Solution
{
    static bool findPrimeNumber(int i)
    {
        for (int j = 2; j * j <= i; ++j)
        {
            if (i % j == 0)
            {
                return false;
            }
        }        
        return true;
    }
    
    public int solution(int n) {
        int answer = 0;        
        for (int i = 2; i <= n; ++i)
        {
            answer = findPrimeNumber(i) == true ? answer + 1 : answer;            
        }
        
        return answer;
    }
}

/*
public int solution(int n)
{
    int answer = 0;
    for (int i = 2; i <= n; ++i)
    {
        for (int j = 2; j * j <= i; ++j)
        {
            if (i % j == 0)
            {
                goto NoPrime;
            }
        }

        ++answer;
        NoPrime:;//고투문 아래 다른 코드 없으면 : 다음에 바로 ; 붙이기!
        }

    return answer;
}
*/
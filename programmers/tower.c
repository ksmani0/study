#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

// heights_len은 배열 heights의 길이입니다.
// return 값은 malloc 등 동적 할당을 사용해주세요. 할당 길이는 상황에 맞게 변경해주세요.
static size_t s_stack_count = 0;
static size_t s_max = 0;

static void push(int* stack, int height)
{
    assert(s_stack_count < s_max);
    stack[s_stack_count++] = height;
}

static int pop(int* stack)
{
    assert(s_stack_count != 0);
    return stack[--s_stack_count];
}

int* solution(int heights[], size_t heights_len) {
    size_t i;
    int* stack = malloc(sizeof(int) * heights_len);
    int* answer = (int*)malloc(sizeof(int) * heights_len);
    s_max = heights_len;

    for (i = 0; i < heights_len; ++i) {
        if (i == 0) {
            push(stack, heights[i]);
            *answer = 0;
            continue;
        }

        while (s_stack_count != 0) {
            int result = pop(stack);
            if (result > heights[i]) {
                *(answer + i) = s_stack_count + 1;
                break;
            }

            if (s_stack_count == 0) {
                *(answer + i) = 0;
            }
        }

        while (s_stack_count <= i) {
            push(stack, heights[s_stack_count]);
        }
    }

    return answer;
}

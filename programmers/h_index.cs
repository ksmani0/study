using System;

public class Solution {
    public static void ReverseSort(int[] arr, int start, int end)
    {
        if (start >= end)
        {//정렬할 요소가 1개뿐이면 끝
            return;
        }
        int pivot = start;//시작점을 기준점으로
        int left = start + 1;//시작점 다음으로 지정. 반복문에서 오른쪽으로 이동
        int right = end;//끝점으로 지정. 반복문에서 왼쪽으로 이동
        
        while (left <= right)
        {//두 색인이 엇갈리기 전까지 돌린다
            while (left <= end && arr[pivot] <= arr[left])
            {//기준보다 작은 값이 오른쪽에 있으면 멈춤
                left++;
            }
            while (right > start && arr[pivot] >= arr[right])
            {//기준보다 큰 값이 왼쪽에 있으면 멈춤
                right--;
            }
            
            if (left > right)
            {//색인이 엇갈리면 기준과 right의 값 교환
                int temp = arr[right];
                arr[right] = arr[pivot];
                arr[pivot] = temp;
            }
            else
            {//색인 엇갈리기 전 교환할 값 찾았으니 교환
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }
        }
        
        ReverseSort(arr, start, right - 1);//앞 부분 정렬
        ReverseSort(arr, right + 1, end);//뒷 부분 정렬
    }
    
    public int solution(int[] citations) {
        int answer = 0;
        ReverseSort(citations, 0, citations.Length - 1);
        if (citations[0] == 0)
        {
            return 0;
        }
        
        for (int i = 0; i < citations.Length; ++i)
        {
            if (citations[i] < i + 1)
            {
                answer = i;
                break;
            }
        }
        return answer == 0 ? citations.Length : answer;
    }
}
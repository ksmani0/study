#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// 리턴할 값은 메모리를 동적 할당해주세요.
int* solution(long long n) {
    long long copy = n;
    char buffer[16];
    char* p = buffer;
    size_t size = 1;
    int* answer;
    int* ap;
    
    while (copy / 10 != 0) {
        sprintf(p, "%d", copy % 10);
        copy /= 10;
        ++size;
        ++p;
    }
    sprintf(p, "%d", copy % 10);
    *(++p) = '\0';
    
    answer = (int*)malloc(size * sizeof(int));
    ap = answer;
    p = buffer;
    while (*p != '\0') {
        *ap = *p - '0';
        ap = *(++p) == '\0' ? ap : ++ap;
    }
    
    return answer;
}

/*
*ap++ = *p++ - '0';//*ap++ 때문에 'heap corruption detected' 오류 발생
*/
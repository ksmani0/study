using System;
using System.Collections.Generic;

public class Solution {
    public int solution(string[,] clothes) {
        int rowSize = clothes.GetLength(0);
        Dictionary<string, int> store = new Dictionary<string, int>();
        
        for (int i = 0; i < rowSize; ++i)
        {
            if (store.TryAdd(clothes[i, 1], 1) == false)
            {
                store[clothes[i, 1]] += 1;
            }
        }
        
        int answer = 1;
        foreach (KeyValuePair<string, int> item in store)
        {
            answer *= (item.Value + 1);
        }
        
        return answer - 1;//스파이가 위장 옷 하나는 무조건 걸쳐야 하니 하나도 안 입는 선택지는 뺌
    }
}
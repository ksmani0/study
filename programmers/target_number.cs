using System;

public class Solution {
    private int result = 0;
    
    public void backtracking(int[] nums, int target, int max, int index)
    {
        if (index == max)//배열 끝까지 음수, 양수 만들기 작업 끝났다면
        {
            int temp = 0;
            for (int i = 0; i < max; ++i)
            {
                temp += nums[i];
            }
            
            if (temp == target)
            {
                result++;
            }
        }
        else
        {
            nums[index] *= -1;//여기서 요소를 음수로 만들고
            backtracking(nums, target, max, index + 1);//다음 색인 넘어감
            nums[index] *= -1;//여기서 다시 양수로 만듦
            backtracking(nums, target, max, index + 1);//이게 없으면 양수 요소로 연산할 수 없음
        }
    }
    
    public int solution(int[] numbers, int target) {
        backtracking(numbers, target, numbers.Length, 0);
        
        return result;
    }
}
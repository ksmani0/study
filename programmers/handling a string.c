#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// 파라미터로 주어지는 문자열은 const로 주어집니다. 변경하려면 문자열을 복사해서 사용하세요.
bool solution(const char* s) {
    const char* p = s;
    size_t count = 1;
    int check_number = 0;
    
    while (*p != '\0') {
        if (*p >= '0'&& *p <= '9') {
            ++check_number;
            if (*(p + 1) == '\0' && ((count == 4 && check_number == 4) || (count == 6 && check_number == 6))) {
                return true;
            }
        } else {
            return false;
        }
        ++count;
        ++p;
    }
    
    return false;
}

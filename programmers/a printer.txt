using System;
using System.Collections.Generic;

public class Solution {
    public int solution(int[] priorities, int location) {
        int size = priorities.Length;
        int standard = priorities[location];
        int biggerCount = 0;

        List<int> bigToZero = new List<int>();
        int max = standard;
        int maxIndex = location;
        for (int i = 0; i < size; ++i)
        {
            if (standard < priorities[i])
            {
                if (max < priorities[i])
                {
                    max = priorities[i];
                    maxIndex = i;
                }
                ++biggerCount;
            }
            bigToZero.Add(priorities[i]);
        }

        int k = 1;
        while (k < biggerCount)
        {
            bigToZero[maxIndex] = 0;

            max = standard;
            for (int j = 0; j < size; ++j)
            {
                if (max < bigToZero[j])
                {
                    max = bigToZero[j];
                }
            }

            for (int j = maxIndex + 1; j < size; ++j)
            {
                if (max == bigToZero[j])
                {
                    bigToZero[j] = 0;
                    maxIndex = j;
                    ++k;
                }
            }
            for (int j = 0; j < maxIndex; ++j)
            {
                if (max == bigToZero[j])
                {
                    bigToZero[j] = 0;
                    maxIndex = j;
                    ++k;
                }
            }
        }

        if (maxIndex < location)
        {
            for (int i = maxIndex; i < location; ++i)
            {
                if (bigToZero[i] == standard)
                {
                    ++biggerCount;
                }
            }
        }
        else if (maxIndex > location)
        {
            for (int i = maxIndex + 1; i < size; ++i)
            {
                if (bigToZero[i] == standard)
                {
                    ++biggerCount;
                }
            }
            for (int i = 0; i < location; ++i)
            {
                if (bigToZero[i] == standard)
                {
                    ++biggerCount;
                }
            }
        }
        else if (maxIndex == location)
        {
            for (int i = 0; i < location; ++i)
            {
                if (bigToZero[i] == standard)
                {
                    ++biggerCount;
                }
            }
        }

        return biggerCount + 1;
    }
}

/*
[프로그래머스]
#코딩 테스트 > 레벨2> 프린터
*/
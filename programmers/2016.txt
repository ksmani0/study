#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

char* solution(int a, int b) {
    int day_count = 0;
    
    switch (a) {
        case 1:
            day_count = 1 + (b - 1);
            break;
        case 2:
            day_count = 32 + (b - 1);
            break;
        case 3:
            day_count = 61 + (b - 1);
            break;
        case 4:
            day_count = 92 + (b - 1);
            break;
        case 5:
            day_count = 122 + (b - 1);
            break;
        case 6:
            day_count = 153 + (b - 1);
            break;
        case 7:
            day_count = 183 + (b - 1);
            break;
        case 8:
            day_count = 214 + (b - 1);
            break;
        case 9:
            day_count = 245 + (b - 1);
            break;
        case 10:
            day_count = 275 + (b - 1);
            break;
        case 11:
            day_count = 306 + (b - 1);
            break;
        case 12:
            day_count = 336 + (b - 1);
            break;
        default:
        break;
    }
    
    day_count %= 7;
    char* answer = (char*)malloc(sizeof(char) * 4);
    
    switch (day_count) {
        case 1:
            strcpy(answer, "FRI");
            break;
        case 2:
            strcpy(answer, "SAT");
            break;
        case 3:
            strcpy(answer, "SUN");
            break;
        case 4:
            strcpy(answer, "MON");
            break;
        case 5:
            strcpy(answer, "TUE");
            break;
        case 6:
            strcpy(answer, "WED");
            break;
        case 0:
            strcpy(answer, "THU");
            break;
        default:
            break;
    }
    
    return answer;
}

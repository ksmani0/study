#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// 파라미터로 주어지는 문자열은 const로 주어집니다. 변경하려면 문자열을 복사해서 사용하세요.
// return 값은 malloc 등 동적 할당을 사용해주세요. 할당 길이는 상황에 맞게 변경해주세요.
char* solution(const char* s) {
    int i;
    size_t size = strlen(s) + 1;
    char* answer = (char*)malloc(size);

    strcpy(answer, s);
    for (i = 0; i < (int)size - 2; ++i) {
        if (answer[i] < answer[i + 1]) {
            char tmp = answer[i];
            answer[i] = answer[i + 1];
            answer[i + 1] = tmp;
            i = -1;
            continue;
        }
    }

    return answer;
}


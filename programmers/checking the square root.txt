[프로그래머스]
#코딩테스트 > 연습문제 > 정수 제곱근 판별

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

long long solution(long long n) {
    long long answer = 0;
    size_t i;
    for (i = 1; i * i <= n; ++i) {
        if (i * i == n) {
            return (i + 1) * (i + 1); 
        }
    }
    return -1;
}

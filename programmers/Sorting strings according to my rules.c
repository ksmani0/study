using System.Collections.Generic;

public class Solution {
    public string[] solution(string[] strings, int n) {
        List<string> orderedList = new List<string>(strings);
        
        for (int i = 0; i < orderedList.Count - 1; ++i)//대문자로 시작해 Count임
        {
            if (orderedList[i][n] > orderedList[i + 1][n])
            {
                string temp = orderedList[i];
                orderedList[i] = orderedList[i + 1];
                orderedList[i + 1] = temp;
                i = -1;
            }
            else if (orderedList[i][n] == orderedList[i + 1][n])
            {
                if (string.Compare(orderedList[i], orderedList[i + 1]) == 1)
                {
                    string temp = orderedList[i];
                    orderedList[i] = orderedList[i + 1];
                    orderedList[i + 1] = temp;
                    i = -1;
                }
            }
        }
                
        return orderedList.ToArray();
    }
}
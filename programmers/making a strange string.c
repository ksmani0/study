#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// 파라미터로 주어지는 문자열은 const로 주어집니다. 변경하려면 문자열을 복사해서 사용하세요.
// return 값은 malloc 등 동적 할당을 사용해주세요. 할당 길이는 상황에 맞게 변경해주세요.
char* solution(const char* s) {
    char* p;
    bool even = true;
    size_t size = strlen(s) + 1;
    char* answer = (char*)malloc(size);
    
    strcpy(answer, s);
    p = answer;
    
    while (*p != '\0') {
        if (*p == ' ') {
            even = true;
        } else if (even) {
            *p = *p <= 'Z' ? *p : *p & ~(char)0x20;
            even = false;
        } else {
            *p = *p <= 'Z' ? *p | (char)0x20 : *p;
            even = true;
        }
        ++p;
    }
    
    return answer;
}

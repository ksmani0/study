[프로그래머스]
#코딩테스트 > 연습문제 > 나누어 떨어지는 숫자 배열

using System.Collections.Generic;

public class Solution {
    public int[] solution(int[] arr, int divisor) {
        int arrLength = arr.Length;
        int digitCount = 0;
        List<int> orderedList = new List<int>(arrLength);

        for (int i = 0; i < arrLength; ++i)
        {
            if (arr[i] % divisor == 0)
            {
                orderedList.Add(arr[i]);
                ++digitCount;
            }
        }

        if (digitCount == 0)
        {
            int[] answer = new int[1];
            answer[0] = -1;
            return answer;
        }

        for (int i = 0; i < digitCount - 1; ++i)
        {
            if (orderedList[i] > orderedList[i + 1])
            {
                int temp = orderedList[i];
                orderedList[i] = orderedList[i + 1];
                orderedList[i + 1] = temp;
                i = -1;
            }
        }

        return orderedList.ToArray();
    }
}